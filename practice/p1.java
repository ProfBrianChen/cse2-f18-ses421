import java.util.Scanner;

public class p1 {
  public static int sum ( int smallInt, int bigInt ){
    int val = 0;
    for (int i = smallInt; i <= bigInt; ++i){
      val = val + i;
    }
    return val;
  }
  
  public static void main (String [] args){
    Scanner scan = new Scanner (System.in);
  
  System.out.print("Enter integer: ");
  int small = scan.nextInt();
  System.out.print("Enter int greater than " + small + " : ");
  int big = scan.nextInt();
   
  while (big <= small){
    System.out.println("Sorry, you entered " + big + " <= " + small + "; try again.");
    System.out.print("Enter an int greater than " + small + " : ");
    big = scan.nextInt();}
  
  int sumVal = sum(small,big);
  
  for (int i = small; i <= big -1; ++i){
    System.out.print(i + " + ");}
  System.out.print(big + " ");
  System.out.println("= " + sumVal);
  }
  
}