///
import java.util.Scanner;

public class nStars {
  public static void main(String[] args){
    
    Scanner scan = new Scanner ( System.in );
    System.out.print("Enter an integer between 1 and 15: ");
    boolean check = false;
    int stars = 0;
    
      while ( check == false ){
        if ( !scan.hasNextInt() ){
        System.out.println("Invalid input! Please enter an integer between 1 and 15: ");
        scan.next();
       }
        else {
          stars = scan.nextInt();
          if ( stars >= 1 && stars<=15 ){
            check = true;
          }
          else {
            System.out.println("Invalid input! Please enter an integer between 1 and 15: ");
          }
        }    
        }

    
    int counter = 1;
    while (counter <= stars){
      for (int i = 1; i <=counter; ++i){
        System.out.print("*");
      }
      System.out.println(" ");
      counter = counter + 1;
    }
  }
}