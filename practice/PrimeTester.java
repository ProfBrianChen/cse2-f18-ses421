///test if a number is prime using a while/for loop

import java.util.Scanner;


public class PrimeTester {
  public static void wloop(){
    Scanner scan = new Scanner( System.in );
    
    System.out.print("Input an integer you believe is prime: ");
    int test = scan.nextInt();
    
    int counter = 2;
    boolean prime = true;
    
    while ( counter < test){
      int check = test%counter;
        if (check == 0){
          System.out.println("The integer is divisible by " + counter);
          counter = counter + test;
          prime = false;
        }
      else {
        counter = counter + 1;
      }
    }
if (prime == true){
  System.out.println("The number is prime!");
}
  }
  
  public static void floop(){
    Scanner scan = new Scanner ( System.in );
    System.out.print("integer: ");
    int test = scan.nextInt();
    
    boolean prime = true;
    
    for ( int counter = 2; counter<test; ++counter){
      int check = test%counter;
      if (check ==0){
        System.out.println("The integer is divisible by" + counter);
        counter = test+1;
        prime = false;
      }
    }
    if (prime == true){
      System.out.println("Prime");
    }
  }
  
  public static void main(String [] args){
    floop();
  }
}