import java.util.Scanner;

public class p3 {
  
  
  public static int sumsq(int num){
    int val = 0;
    for (int i = 1; i <=num; ++i){
      val = val + i*i;
    }
    return val;
  }
  public static void main (String [] args){
    Scanner scan = new Scanner ( System.in );
    int val = 0;
    boolean check = true;
    do {
      System.out.print("Enter an integer: ");
      int n = scan.nextInt();
      if (n<=0){
        check = false;
      }
      if (n>0){
        val = sumsq(n);
        System.out.println("The sum of 1+2*2+3*3+...n*n for n = " + n + " is " + val);
      }
    } while ( check == true);
  }
}