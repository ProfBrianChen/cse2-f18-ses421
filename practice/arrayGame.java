//
import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class arrayGame {
  public static void main (String[] args){
    
    Scanner scan = new Scanner(System.in);
    Random rand = new Random();
    
    System.out.print("How many numbers do you want to guess? ");
    int num = scan.nextInt();
    int[] array = new int[num]; //declare array with inputted length
    
    for (int i = 0; i < num; i++){
      int random = rand.nextInt(10);
      array[i]=random;
    }
    Arrays.sort(array);
    System.out.println(Arrays.toString(array));
  }
}