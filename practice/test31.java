import java.util.Arrays;

//
public class test31 {
  
  public static double[] expand(double[] A, double[] B, double[] C){
    int length = A.length+B.length+C.length;
    double[] D = new double[length];
    for(int i = 0; i < A.length; i++){
      D[i] = A[i];
    }
    for(int i = 0; i<B.length; i++){
      D[i+A.length] = B[i];
    }
    for(int i = 0; i<C.length; i++){
      D[i+A.length+B.length] = C[i];
    }
    return D;
  }
  
  public static void main ( String[] args) {
    double[] test1 = {20, 4, 7, 13};
    double[] test2 = {1, 2, 3, 4, 5, 6};
    double[] test3 = {1, 2, 3, 4, 5,6, 3, 2, 1};
    double[] array = expand(test1, test2, test3);
    System.out.println(Arrays.toString(array));
  }
}