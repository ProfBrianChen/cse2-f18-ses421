//
import java.util.Scanner;

public class starPyr {
  public static void main(String [] args){
    Scanner scan = new Scanner ( System.in );
    
    System.out.print("Height: ");
    int height = scan.nextInt();
    int counter = 1;
    while ( counter <= height ){
      int spaces = height - counter;
      for (int i=1; i <= spaces; ++i ){
        System.out.print(" ");
      }
      int stars = 1 + (2*(counter-1));
      for (int j = 1; j<=stars; ++j){
        System.out.print("*");
      }
      System.out.println(" ");
      counter = counter + 1;
    }
  }
}