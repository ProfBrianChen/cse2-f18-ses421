//Sophie Smith
//CSE02-311
//10-9-2018
// Produce a pattern that has an X in it
//
import java.util.Scanner; //import scanner class to use it
//
public class EncryptedX {
  //main method for every java program
  public static void main (String[] args){
    
    Scanner scan = new Scanner( System.in ); //tell the scanner I am creating an instance that will take input from STDIN
    System.out.print("Enter an integer 0-100: "); // get integer value from the user
     int input = 0; // initialize a value for the input
    boolean check = false; //initialize a boolean as false to check against
    
    //check to see if the user has inputted a valid input
    while ( check == false ){ //while the boolean is false
      if (!scan.hasNextInt()){ //check if the scan was an integer
        System.out.println("That is not a valid input. Please enter a valid input:"); //if it's not, ask for a new integer
        scan.next(); //scan again until it is an integer
      }
      else{
       input = scan.nextInt(); // set input equal to what the user put in
        if (input >= 0 && input <= 100){ //check if the number is between 0 and 100
          check = true; //if so, set boolean to true so the loop doesn't run again
        }
        else{ //if its not
          System.out.println("That is not a valid input. Please enter a valid input:"); //ask for a new input, and the loop continues
        }
      }
      }
    
    //print out pattern
     for(int numRows = 1; numRows <= input; numRows++){ //while the user input is less than the number of rows printed
      int count = 1; //initialize a count for printing the pattern
        while ( count <= input ){ //while the counter is less than the input
          int spaces = numRows; //initialize a variable to identify where the space should be
          int val = (input+1) - count; //initialize a value to use later         
          
          if ( spaces == count ){ //if the number is equal to the current count
            System.out.print(" "); //print a space
          }
          else if ( spaces == val && spaces != count){ //if its equal to the input minus the count, starting at 1
            System.out.print(" "); //print the further space
          }
          else { //if it's not time to print a space
          System.out.print("*"); //print out a star
        }
          count = count+1; //add one to count, star will be printed again
          }
      count = 1; //set counter back to one
      System.out.println(" "); //on to the next row
    }
    
  }
}

