// Sophie Smith
// CSE 02-311
// 9/13/2018
///
import java.util.Scanner; //import scanner class to use it
//
public class Check {
  // main method required for every Java program
  public static void main(String[] args) {
    //collect all the input data
    Scanner myScanner = new Scanner( System.in ); //tell the scanner I am creating an instance that will take input from STDIN
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //prompt user for the original cost of the check
      double checkCost = myScanner.nextDouble(); //call a method to accept the double inputed
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //prompt user for the tip percentage they wish to pay
      double tipPercent = myScanner.nextDouble(); //call a method to accept the double inputed
        tipPercent /= 100; //convert the percentage into a decimal value and restore it in tipPercent
    System.out.print("Enter the number of people who went to dinner: "); //prompt user for number of people who went to dinner
      int numPeople = myScanner.nextInt(); // call a method to accept an integer inputed
    //develop and print the output
      double totalCost; //declare the total cost as a double variable
      double costPerPerson; //declare the cost per person as a double variable
      int dollars, dimes, pennies; //declare integer values for the whole dollar amount and to store digits to the right of the decimal point for the cost 
    totalCost = checkCost * (1 + tipPercent); //calculate total cost by adding the tip to the original cost
    costPerPerson = totalCost / numPeople; //gets the whole amount for each person with too many decimals
      dollars = (int) costPerPerson; //gets dollar amounts
      dimes = (int) (costPerPerson * 10) % 10; //gets the dime amounts
      pennies = (int) (costPerPerson * 100) % 10; //gets the amount of pennies
    System.out.println("Each person in the group owes $"+dollars+"."+dimes+""+pennies+""); //prints out the total amount as increments    
  } //end of main method
} //end of class