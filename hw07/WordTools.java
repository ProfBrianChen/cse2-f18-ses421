//Sophie Smith
//CSE02-311
//10-28-2018
// allow user to input and then edit/analyze a sentence
//
import java.util.Scanner; //import scanner class to use it

public class WordTools { //public class
  
  public static String sampleText(){ //method to prompt user to input sample text
    Scanner scan = new Scanner( System.in ); // import the scanner into the method
      System.out.println("Enter a sample text: "); //ask user to enter a string
      String userText = scan.nextLine(); //save the user's input as a string
    
    System.out.println(" ");
    System.out.println("You entered: " + userText); //print back what the user entered as a string
    return userText; //return the string to the main method
  }
  
  public static char printMenu(){ //method that displays the menu and makes user enter a character input
    //print out the menu and all the options for the user
    System.out.println("MENU:");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println(" ");
    
    Scanner scan = new Scanner( System.in ); //input scanner in the method
    System.out.println("Choose an option: "); //ask user to choose an option
    
    String selection = scan.next(); //take user input as string
    char selectionChar = 'a'; //initialize character for selection
    
    if ( selection.length() != 1 ){ //if the user inputted more than one letter
      selectionChar = 'a'; //set the char to an invalid input so the user will be prompted again
    }
    else { //if the user inputted one letter
      selectionChar = selection.charAt(0); //set the character equal to the letter
    }
    
    return selectionChar; //return the character the user selected
  }
  
  public static int getNumOfNonWSCharacters(String text){
    int upperLimit = text.length() -1; //initialize upper limit for loop at the place of final character
    int counter = 0; //initialize counter
    
    for ( int i = 0; i <= upperLimit; i++){ //count up to the upper limit of the string
      char check = text.charAt(i);
      if ( !Character.isWhitespace(check) ){ //check if the character at position i is NOT whitespace
        counter = counter + 1; //if so, add one to counter
      }
    }
    return counter; //return the final value of the counter
  }
  
  public static int getNumOfWords(String text){
    int upperLimit = text.length() -1; //initialize upper limit for loop at the place of final character
    int counter = 0; //initialize counter
    
    for ( int i = 0; i <= upperLimit; i++){ //count up to the upper limit of the string
      char check = text.charAt(i);
      if ( Character.isWhitespace(check) ){ //check if the character at position i is whitespace
        char check2 = text.charAt(i-1); //check the character before
        if ( !Character.isWhitespace(check2)){ //if the previous character ISNT a space
        counter = counter + 1; //if so, add one to counter bc then a word has just finished
        }
      }
    }
    counter = counter + 1; //account for the last word in the string
    return counter; //return the final value of the counter
  }
  
  public static int findText( String input, String text ){
    int counter = 0; //initialize the counter
    
    if ( text.contains(input) ){ //check if the user input is contained within the string of text
      int length1 = text.length(); //get the length of the text
      String text2 = text.replace(input, ""); //replace all the inputs with nothing
      int length2 = text2.length(); // get the length after removing all of the inputs
      counter = (length1-length2)/ (input.length()); //gives the number of times the input was in the String
    }
    return counter; //return the counter
  }
  
  public static String replaceExclamation( String text ){ //method to replace all the exclamation points
      String editedText = text.replace("!", "."); //replace exclamation points with periods within the string
      return editedText; //return the edited text to the main method
  }
  
  public static String shortenSpaces( String text ){
    String editedText = text.replace("  ", " "); //replace any double spaces with one space
    return editedText; // return the edited text
  }
  
  public static void main (String [] args){
    
    Scanner scan = new Scanner(System.in); //import scanner to method
    String text = sampleText();
    int counter = 0; //initialize counter to check for valid character
    
    while ( counter == 0){ //while the counter is at 0
      char character = printMenu(); //initialize character returned from the print menu method
        switch (character){ //check what the user inputted character is 
          case 'q': //if it's q
            counter = 1; //set counter equal to one to quit the program
            break;
          case 'c': //if the user wants to get the number of non whitespace characters
            int numC = getNumOfNonWSCharacters(text); //call the method
            System.out.println("Number of non-whitespace characters: " + numC); //print out the number of non white space characters
            break; //go back to the menu
          case 'w':
            int numW = getNumOfWords(text); //call the method to count the number of words
            System.out.println("Number of words: " + numW); //print out the number of words
            break;
          case 'f':
            System.out.print("Enter a word or phrase to be found: "); //ask user what word they want to find
            String input = scan.nextLine(); //initialize the input
            int numF = findText(input, text); //run the find text method and count the instances
            System.out.println("\"" + input + "\"" + " instances: " + numF); //print out number 
            break;
          case 'r': //if they want to replace all the exclamation points with periods
            String periods = replaceExclamation(text); // call the method
            System.out.println("Edited text: " + periods); //print out the edited text
            break;
          case 's': //if they want to shorten spaces
            String spaces = shortenSpaces(text); //run the shorten spaces method
            System.out.println("Edited text: " + spaces); //print the edited text
            break;
          default: //if the user inputted something else
            System.out.println("You have entered an invalid input. Please select from the given menu."); //ask them to try again
            break; //re enter the print menu method
        }
    }
    
  }
}