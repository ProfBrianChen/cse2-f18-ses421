//Sophie Smith
//CSE02-311
//10-9-2018
// Produce a pattern with the number of lines that the user inputs
//
import java.util.Scanner; //import scanner class to use it
//
public class PatternC {
  //main method for every java program
  public static void main (String[] args){

    Scanner scan = new Scanner( System.in ); //tell the scanner I am creating an instance that will take input from STDIN
    System.out.print("Number of rows: "); //ask the user how many lines
     int input = 0; 
    boolean check = false;
    
    while ( check == false ){
      if (!scan.hasNextInt()){
        System.out.println("That is not a valid input. Please enter a valid input.");
        scan.next(); //scan again until it is an integer
      }
      else{
       input = scan.nextInt(); //set courseNum equal to integer
        if (input >= 1 && input <= 10){
          check = true;
        }
        else{
          System.out.println("That is not a valid input. Please enter a valid input.");
        }
      }
      }
    
    for(int numRows = 1; numRows <= input; numRows++){ //while the user input is less than the number of rows printed
      int count = 1; //initialize a count for printing the pattern
      int spaces;
      spaces = input - numRows;
        while ( spaces > 0){
          System.out.print(" ");
          spaces = spaces -1;
        }
      int count2;
      count2 = numRows;
        while ( count <= numRows ){ //while teh counter is less than the number of rows printed
          System.out.print(count2); //print out the count, starting at 1
          count2 = count2-1; //add one to count
          count = count+1;
        }
      count = 1; //set counter back to one
      System.out.println(" "); //on to the next row
    }
    
  }
}