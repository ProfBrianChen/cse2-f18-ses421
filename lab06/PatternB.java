//Sophie Smith
//CSE02-311
//10-9-2018
// Produce a pattern with the number of lines that the user inputs
//
import java.util.Scanner; //import scanner class to use it
//
public class PatternB {
  //main method for every java program
  public static void main (String[] args){

    Scanner scan = new Scanner( System.in ); //tell the scanner I am creating an instance that will take input from STDIN
    System.out.print("Number of rows: "); //ask the user how many lines
    
     int input = 0; 
    boolean check = false;
    
    while ( check == false ){
      if (!scan.hasNextInt()){
        System.out.println("That is not a valid input. Please enter a valid input.");
        scan.next(); //scan again until it is an integer
      }
      else{
       input = scan.nextInt(); //set courseNum equal to integer
        if (input >= 1 && input <= 10){
          check = true;
        }
        else{
          System.out.println("That is not a valid input. Please enter a valid input.");
        }
      }
      }
        
    int count2 = input;
    
    for(int numRows = 1; numRows <= input; numRows++){ //while the user input is less than the number of rows printed
      int count = 1; //initialize a count for printing the pattern
        while ( count <= count2 ){ //while teh counter is less than the number of rows printed
          System.out.print(count); //print out the count, starting at 1
          count = count+1; //add one to count
        }
      count = 1; //set counter back to one
      count2 = count2 -1;
      System.out.println(" "); //on to the next row
    }
    
  }
}