//Sophie Smith
//CSE02-311
//9-20-2018
//Create a program using only switch statements that plays craps
//
import java.util.Scanner; //import scanner class to use it
//
public class CrapsSwitch {
  //main method for every java program
  public static void main (String[] args){
   //ask the user what they want to do
    Scanner myScanner = new Scanner( System.in ); //tell the scanner I am creating an instance that will take input from STDIN
    System.out.print("Type 'random' to randomly cast dice or type 'state' if you want to state the two dice values: "); //prompt user if they want to randomly cast dice or if they'd like to state the two dice they want to evaluate
      String diceType = myScanner.next(); //call a method to accept the string inputed
    //set values on dice if the user chooses to go RANDOM
    int dieOne = 0; //initialize a variable for the number on die one
    int dieTwo = 0; //initialize a variable for the number on die two
    String input = "valid input"; //initialize a string to use later to determine if the slang term should be printed
    switch ( diceType ){ //reads the string diceType
      case "random": //if user inputted random
        dieOne = (int)(Math.random()*6)+1; //set die one to a random integer 1-6
        dieTwo = (int)(Math.random()*6)+1; //set die two to a random integer 1-6
        break;
      case "state": //if user inputted state
       System.out.print("Integer number on the first die (1-6): "); //ask for user input for the value on die 1
        dieOne = myScanner.nextInt(); //call a method to accept the integer inputted
       System.out.print("Integer number on the second die (1-6): "); //ask for user input for the value on die 2
        dieTwo = myScanner.nextInt(); //call a method to accept the integer inputted
        //check if the values entered are valid
          switch ( dieOne ){ //read the value of die one
            case 1: //if it equals 1
            case 2: //or 2 
            case 3: //or 3 
            case 4: //or 4
            case 5: //or 5 
            case 6: //or 6
              break;
            default:
              input = "invalid input"; //change the string term to invalid will be used later to tell the program not to print
              System.out.println("Invalid input!"); //print that the input is invalid
              break;
            }
           switch ( dieTwo ){ //same thing as above but for die 2 
             case 1:
             case 2:
             case 3: 
             case 4:
             case 5:
             case 6:
               break;
             default:
              input = "invalid input"; //change the string term to invalid will be used later to tell the program not to print
              System.out.println("Invalid input!"); //print that the input is invalid
              break;
               }
        break;
      default: //if user typed something else
        input = "invalid input"; //change string term to invalid
        System.out.println("Invalid input!"); //print that the input is invalid
        break;
       }
    //determine what the slang term is for the values on the dice
    String slangTerm = "Slang term"; //initialize a string variable to hold the slang term
    switch ( dieOne + dieTwo ){
      case 2: //if the dice sum to 2
        slangTerm = "Snake eyes"; //called snake eyes
        break;
      case 3: //if the dice sum to 3
        slangTerm = "Ace deuce"; //called ace deuce
        break;
      case 4: //if the dice sum to 4
        switch ( dieOne ){ //another switch to check if both dice are 2s - can only check one because if one is 2, the other will be 2 also
          case 2: //if one die is a 2
             slangTerm = "Hard four"; // then they are both 2s and it's a hard four
             break;
           default: //if one isn't a 2
             slangTerm = "Easy four"; //called easy 4
             break;
            }
        break;
      case 5: //if the dice sum to five
        slangTerm = "Fever five"; //called fever five
        break;
      case 6: //if the dice sum to 6
        switch ( dieOne ){ //another switch to check if both dice are 3s - can only check one because if one is 3, the other will be 3 also
          case 3: //if one die is a 3
             slangTerm = "Hard six"; // then they are both 3s and it's a hard six
             break;
           default: //if one isn't a 3
             slangTerm = "Easy six"; //called easy six
             break;
            }
        break;
      case 7: //if the dice sum to 7
        slangTerm = "Seven out"; //called seven out
        break;
      case 8: //if the dice sum to 8
        switch ( dieOne ){ //another switch to check if both dice are 4s - can only check one because if one is 4, the other will be 4 also
          case 4: //if one die is a 4
             slangTerm = "Hard eight"; // then they are both 4s and it's a hard eight
             break;
           default: //if one isn't a 4
             slangTerm = "Easy eight"; //called easy eight
             break;
            }
        break;
      case 9: //if the dice sum to 10
        slangTerm = "Nine"; //called a nine
        break;
      case 10: //if the dice sum to 10
        switch ( dieOne ){ //another switch to check if both dice are 5s - can only check one because if one is 5, the other will be 5 also
          case 5: //if one die is a 5
             slangTerm = "Hard ten"; // then they are both 5s and it's a hard ten
             break;
           default: //if one isn't a 5
             slangTerm = "Easy ten"; //called easy ten
             break;
            }
        break;
      case 11: //if the dice sum to 11
        slangTerm = "Yo-leven"; //called yo-leven haha
        break;
      case 12: //if the dice sum to 12
        slangTerm = "Boxcars"; //called boxcars
        break;   
      default: //if none of these cases occur
        break;
    }
  //print out results if the inputs were valid
  if ( !input.equals("invalid input") ){ //if the inputs were all valid
       System.out.println(slangTerm); //prints the slang term
    }
  } //end of main method 
} //end of class