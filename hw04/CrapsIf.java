//Sophie Smith
//CSE02-311
//9-20-2018
//Create a program using only if and else statements that plays craps
//
import java.util.Scanner; //import scanner class to use it
//
public class CrapsIf {
  //main method for every java program
  public static void main (String[] args){
   //ask the user what they want to do
    Scanner myScanner = new Scanner( System.in ); //tell the scanner I am creating an instance that will take input from STDIN
    System.out.print("Type 'random' to randomly cast dice or type 'state' if you want to state the two dice values: "); //prompt user if they want to randomly cast dice or if they'd like to state the two dice they want to evaluate
      String diceType = myScanner.next(); //call a method to accept the string inputed
    //set values on dice if the user chooses to go RANDOM
    int dieOne = 0; //initialize a variable for the number on die one
    int dieTwo = 0; //initialize a variable for the number on die two
    if ( diceType.equals("random") ){ //if the user inputted random
      dieOne = (int)(Math.random()*6)+1; //generate a random number between 1 and 7 for die 1
      dieTwo = (int)(Math.random()*6)+1; //generate a random number between 1 and 7 for die 2
    }
    //set values on dice if the user chooses to state the values
    if ( diceType.equals("state") ){ //if the user inputted state
      System.out.print("Integer number on the first die (1-6): "); //ask for user input for the value on die 1
        dieOne = myScanner.nextInt(); //call a method to accept the integer inputted
      System.out.print("Integer number on the second die (1-6): "); //ask for user input for the value on die 2
        dieTwo = myScanner.nextInt(); //call a method to accept the integer inputted 
    }
    //check if the numbers are in range
    String invalidInput = "valid input"; //create a string to later see if it should print the slang term; if this changes to invalid
    if ((dieOne != 1 && dieOne != 2 && dieOne != 3 && dieOne != 4 && dieOne != 5 && dieOne != 6) || (dieTwo != 1 && dieTwo != 2 && dieTwo != 3 && dieTwo != 4 && dieTwo != 5 && dieTwo != 6)){ //check if either die equals something that isn't integer 1-6, or if the user just didn't type state or random so the dice never were assigned valid values
      invalidInput = "invalid input"; //if one is an invalid number, change the string to invalid
      System.out.println("Invalid input!"); //print that the input is invalid
    }
    //generate slang term
    String slangTerm = "slang term"; //initialize variable to hold slang term
    if ( dieOne == 1 && dieTwo == 1 ){ //if both dice show 1
      slangTerm = "Snake eyes"; //called snake eyes
    }
    if ( dieOne + dieTwo == 3 ){ //if the two dice show a 1 and 2
      slangTerm = "Ace deuce"; //called an ace deuce
    }
    if ( dieOne == 2 && dieTwo == 2 ){ //if both dice show 2
      slangTerm = "Hard four"; //called hard four
    }
    if ( ( dieOne == 1 && dieTwo == 3 ) || ( dieOne == 3 && dieTwo == 1 )){ //if the dice show a 1 and a 3
      slangTerm = "Easy four"; //called easy 4
    }
    if ( dieOne + dieTwo == 5 ) { //if the sum of the dice is 5
      slangTerm = "Fever five"; //called fever five
    }
    if ( ( dieOne + dieTwo == 6 ) && ( dieOne != 3 && dieTwo !=3)) { //if the sum of the dice is 6, but they're not both threes
      slangTerm = "Easy six"; //called easy six
    }
    if ( dieOne == 3 && dieTwo ==3) { //if the dice are both threes
      slangTerm = "Hard six"; //called hard six
    }
    if ( dieOne + dieTwo == 7 ) { //if the sum of the dice is seven
      slangTerm = "Seven out"; //called seven out
    }
    if ( ( dieOne + dieTwo == 8 ) && ( dieOne != 4 && dieTwo !=4)) { //if the sum of the dice is 8 but they're not both 4 
      slangTerm = "Easy eight"; //called easy 8
    }
    if ( dieOne == 4 && dieTwo ==4) { //if both dice show 4
      slangTerm = "Hard eight"; //called a hard 8 
    }
    if ( dieOne + dieTwo == 9 ) { //if the sum of the dice is 9 
      slangTerm = "Nine"; //called nine
    }
    if ( ( dieOne + dieTwo == 10 ) && ( dieOne != 5 && dieTwo !=5)) { //if the sum of the dice is 10 but they're not both 5s 
      slangTerm = "Easy ten"; //called easy 10
    }
    if ( dieOne == 5 && dieTwo ==5) { //if they both show 5s 
      slangTerm = "Hard ten"; //called hard 10 
    }
    if ( dieOne + dieTwo == 11 ) { //if the sum of the dice is 11
      slangTerm = "Yo-leven"; //called yo-leven haha
    }
    if ( dieOne + dieTwo == 12 ) { //if the sum of the dice is 12
      slangTerm = "Boxcars"; //called boxcars
    }
    //print out the slang term if the inputs were valid
    if ( !invalidInput.equals("invalid input") ){ //if the inputs were all valid
       System.out.println(slangTerm); //prints the slang term
    }
  } //end of class 
} //end of main method