//Sophie Smith
//CSE02-311
//11-13-2018
// take a deck of cards, shuffle it, and print out random hands for the user
//

import java.util.Scanner; //import scanner to use
import java.util.Random; //import random machine to use

public class hw08 {
  
  public static void printArray( String[] elements ){ //take the string of cards, return nothing
    int len = elements.length; //get the length of the string that is entered in the print array
    for (int i = 0; i < len; i++){ //count up to 52
      System.out.print(elements[i] + " "); //print each element with a space in between
    }
    System.out.println(); //next line
  }
  
  public static void shuffle( String[] elements ){ //take a string to shuffle, return nothing
    Random rand = new Random(); //initialize random machine
    for (int count = 0; count < 200; count++){ //do this 200 times to really shuffle them well
      String zero = elements[0]; //make a string to hold the first element in the string array of cards
      int place = rand.nextInt(52); //randomly select another element in the string array
      elements[0] = elements[place]; //make the zeroth place the same as the one in the randomly selected place
      elements[place] = zero; //put the first element in the string array in the place that was randomly selected
    }
  }
  
  public static String[] getHand( String[] cards, int index, int numCards){ //take two integers and a string array, return another string array
    if (index <= numCards){ //if the index is less than or equal the the number of cards - the loop would go negative
      shuffle(cards); //reshuffle the cards to get new hands
      index = 51; //set index back to 51
    }
    System.out.println("Hand"); //print out Hand
    String[] array1 = new String[5]; //initialize a new string array to hold the hand
    for (int i = 0; i < numCards; i++){ //count up to the number of cards in the hand
      array1[i] = cards[index]; //set the element in the array equal to the card at the index point
      index = index - 1; //subtract one from the index to get five different cards
    }
    return array1; //return the array of the hand to the main method
  }
  
  public static void main(String[] args) {  //PROVIDED TO US BY YOU
    
    Scanner scan = new Scanner(System.in); 
      //suits club, heart, spade or diamond 

    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    
    int numCards = 5; 
    int again = 1; 
    int index = 51;
    
  for (int i=0; i<52; i++){ 
    cards[i]=rankNames[i%13]+suitNames[i/13]; 
  } 
    
  printArray(cards); 
  shuffle(cards);
  System.out.println("Shuffled");
  printArray(cards); 
    
  while(again == 1){ 
    hand = getHand(cards,index,numCards); 
    printArray(hand);
    index = index - numCards;
    System.out.println("Enter a 1 if you want another hand drawn"); 
    again = scan.nextInt(); 
    } 
    
  } 
}
