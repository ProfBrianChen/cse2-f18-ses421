//Sophie Smith
//CSE02-311
//10-9-2018
//Create a program that calculates the probability of various poker hands
//
import java.util.Scanner; //import scanner class to use it
//
public class Hw05 {
  //main method for every java program
  public static void main (String[] args){
   //ask the user what they want to do
    Scanner scan = new Scanner( System.in ); //tell the scanner I am creating an instance that will take input from STDIN
    System.out.print("How many hands should be generated? "); //ask the user to input how many hands to generate
    int hands = scan.nextInt(); //read the value the user inputted for number of hands
    int counter = 0; //set the counter to 0
    int success = 0; // set success counter to 0
    int card1 = 0; //initiates an integer value for each card
    int card2 = 0;
    int card3 = 0;
    int card4 = 0;
    int card5= 0;
    int fourOfAKind = 0; //innitialize integer values for each hand happening and starts them all at zero
    int threeOfAKind = 0;
    int twoPairs = 0;
    int onePair = 0;
   while ( counter < hands ) { //check if the counter is less than the number of hands
      card1 = (int)(Math.random()*52)+1; // generate a random integer between 1 and 52 for card one
        int checkCard2 = 0; //integer to check card 2
        while (checkCard2 == 0){
          card2 = (int)(Math.random()*52)+1; //random card 2
           if (card1 != card2){ //if it isn't the same as card 1
            checkCard2 = 1; // add one to break out of the loop
           }
        }
      int checkCard3 = 0; //same thing for card 3
          while (checkCard3 == 0){
          card3 = (int)(Math.random()*52)+1;
           if (card2 != card3){
            checkCard3 = 1;
           }
        }
     int checkCard4 = 0; //same thing for card 4
          while (checkCard4 == 0){
          card4 = (int)(Math.random()*52)+1;
           if (card3 != card4){
            checkCard4 = 1;
           }
        }
     int checkCard5 = 0; //same thing for card 5
          while (checkCard5 == 0){
          card5 = (int)(Math.random()*52)+1;
           if (card4 != card5){
            checkCard5 = 1;
           }
            
            
            int realNum1 = ( card1 % 13 ); //initialize real number of card
            int realNum2 = ( card2 % 13 ); //initialize real number of card
            int realNum3 = ( card3 % 13 ); //initialize real number of card
            int realNum4 = ( card4 % 13 ); //initialize real number of card
            int realNum5 = ( card5 % 13 ); //initialize real number of card
            
           if (((realNum1 == realNum2) && (realNum2 == realNum3) && (realNum3 == realNum4)) || 
               ((realNum1 == realNum2) && (realNum2 == realNum3) && (realNum3 == realNum5)) || 
               ((realNum1 == realNum2) && (realNum2 == realNum4) && (realNum4 == realNum5)) || 
               ((realNum1 == realNum3) && (realNum3 == realNum4) && (realNum4 == realNum5)) || 
               ((realNum2 == realNum3) && (realNum3 == realNum4) && (realNum4 == realNum5))){ //check if there is a 4 of a kind
                   fourOfAKind += 1; //if there is, add one to the four of a kind counter
                 }           
            else if(((realNum1 == realNum2) && (realNum2 == realNum3)) || 
                    ((realNum1 == realNum2) && (realNum2 == realNum4)) || 
                    ((realNum1 == realNum2) && (realNum2 == realNum5)) || 
                    ((realNum1 == realNum3) && (realNum3 == realNum4)) || 
                    ((realNum1 == realNum3) && (realNum3 == realNum5)) || 
                    ((realNum3 == realNum4) && (realNum4 == realNum5)) || 
                    ((realNum2 == realNum3) && (realNum3 == realNum4)) || 
                    ((realNum2 == realNum3) && (realNum3 == realNum5)) || 
                    ((realNum1 == realNum4) && (realNum4 == realNum5)) || 
                    ((realNum2 == realNum4) && (realNum4 == realNum5))){ //if it's not a 4 of a kind, check if its a three of a kind
                   threeOfAKind += 1; //if there is, add one to three of a kind counter
                 }         
            else if (((realNum1 == realNum2) && (realNum3 == realNum4)) || 
                     ((realNum1 == realNum2) && (realNum3 == realNum5)) || 
                     ((realNum1 == realNum2) && (realNum4 == realNum5)) || 
                     ((realNum1 == realNum3) && (realNum2 == realNum5)) || 
                     ((realNum1 == realNum3) && (realNum2 == realNum4)) || 
                     ((realNum1 == realNum3) && (realNum4 == realNum5)) || 
                     ((realNum1 == realNum4) && (realNum2 == realNum3)) || 
                     ((realNum1 == realNum4) && (realNum2 == realNum5)) || 
                     ((realNum1 == realNum4) && (realNum3 == realNum5)) || 
                     ((realNum1 == realNum5) && (realNum2 == realNum3)) || 
                     ((realNum1 == realNum5) && (realNum2 == realNum4)) || 
                     ((realNum1 == realNum5) && (realNum3 == realNum4))){ //if not, check if there are two pairs
                   twoPairs += 1; //if there are, add one to two pairs counter
                 }            
            else if ((realNum1 == realNum2) || (realNum1 == realNum3) || 
                     (realNum1 == realNum4) || (realNum1 == realNum5) || 
                     (realNum2 == realNum3) || (realNum2 == realNum4) || 
                     (realNum2 == realNum5) || (realNum3 == realNum4) || 
                     (realNum3 == realNum5) || (realNum4 == realNum5)){ //if not, check for just one pair
                   onePair += 1; //if there is one, add one to one pair counter
                 }
            
        counter += 1; //add one to counter to signify this has happened once for one hand, and that the whlile loop has occured one more time
            
        }
   }
    
    //calculate probabilities of each hand
    double probFourOfAKind = ((double) fourOfAKind) / ((double) hands);
    double probThreeOfAKind = ((double) threeOfAKind) / ((double) hands);
    double probTwoPairs = ((double) twoPairs) / ((double) hands);
    double probOnePair = ((double) onePair) / ((double) hands);
    
    //print out probabilities

    System.out.println("The number of loops: " + hands);
    System.out.printf("The probability of getting a 4 of a kind is: %.3f %n", probFourOfAKind);
    System.out.printf("The probability of getting a 3 of a kind is: %.3f %n", probThreeOfAKind);
    System.out.printf("The probability of getting two pairs is: %.3f %n", probTwoPairs);
    System.out.printf("The probability of getting one pair is: %.3f %n", probOnePair);
    }
}
    