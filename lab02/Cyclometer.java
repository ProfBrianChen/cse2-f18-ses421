// Given time and rotation, print number of minutes, counts, distance per trip, and distance for combine trips
// Sophie Smith, 9/6/2018, CSE02-311
//
public class Cyclometer {
  //main method required for every Java program
  public static void main (String[] args) {
  //our input data
    int secsTrip1=480; //length of trip 1 in seconds
    int secsTrip2=3220; //length of trip 2 in seconds
    int countsTrip1=1561; //rotation counts for trip 1
    int countsTrip2=9037; //rotation counts for trip 2
  //our intermediate variables and output data
    double wheelDiameter=27.0; //constant wheel diameter
    double PI=3.14159; //constant for pi
    int feetPerMile=5280; //constant for feet per mile
    int inchesPerFoot=12; //constant for inches per foot
    double secondsPerMinute=60; //constant for seconds per minute
    double distanceTrip1, distanceTrip2, totalDistance; //creating variables for distances that will later store data
  //print out the numbers stored in variables
  //converts seconds to minutes and prints minutes and counts for each trip
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
  //run the calculations; store the value document the calculation here
  //calculate the distance of each trip by finding the circumference of the wheel and seeing how many times it rolled around
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    //above gives distance in inches
    //for each count, a rotation of the wheel travels the diameter in inches times PI
    distanceTrip1/=inchesPerFoot*feetPerMile;
    //above gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    //gives distance for trip 2 in miles
    totalDistance=distanceTrip1+distanceTrip2;
  //print out the output data
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
  } //end of main method
} //end of class