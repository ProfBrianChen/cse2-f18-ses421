///
//
import java.util.Scanner;

public class Arrays {
  
  public static int[] copy( int[] array ){ //method to copy arrays
    int len = array.length; //get the length of the input array
    int[] copy = new int[len]; //make a copy array with the same length as the first array
    
    for (int i = 0; i < len; i++){ //counting up to the length of the array
      copy[i]=array[i]; //each element of the copied array is the element of the first array
    }
    
    return copy; //return the copied array
  }
  
   public static void inverter(int[] array){ //flips comps in array
    int len = array.length/2; //get the length of half the list
    for (int i = 0; i < len; i++){ //counting up to half the list
      int swap = array[i]; //store the integer in place i
      array[i] = array[array.length-1-i];
      array[array.length-1-i] = swap;
    }
  }
  
  public static int[] inverter2(int[] array){
    int[] copy = copy(array);   // pulls a copy of the array from first method
    int len = copy.length/2; //get the length of half the copy
    for (int i = 0; i < len; i++){
      int swap = copy[i]; //swap places of componenets
      copy[i] = copy[copy.length-1-i];
      copy[copy.length-1-i] = swap;
    }
    return copy;   // return the copy to the main method
  }
  
  public static void print( int[] array ){ //method to print arrays
    int len = array.length; //get length
    for (int i = 0; i < len; i++){ //count up to length of array
      System.out.print(array[i] + " "); //print each element with a space in between
    }
    System.out.println(); //next line
  }
  
  public static void main ( String[] args ) {
    int[] array = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
    int[] array0 = copy(array);
    int[] array1 = copy(array);
    int[] array2 = copy(array);
    inverter(array0); //pass through array0
    print(array0); //print array 0
    inverter2(array1);//pass through invertyer
    print(array1); //print array1
    int[] array3 = inverter2(array2); //create a new array that passes array 3 through inverter2
    print(array3); //print out array 3   
  }
}