///////////////
//// CSE 02 Welcome Class
///

public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints welcome and lehigh network ID
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-S--E--S--4--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v  ");
    System.out.println("My name is Sophie Smith and I am from Minneapolis, MN.");
  }
}