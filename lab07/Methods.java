//Sophie Smith
//CSE02-311
//10-9-2018
// generate random sentences
//
import java.util.Scanner; //import scanner class to use it
import java.util.Random; //import random class
//
public class Methods {
  
  public static String adjective(){ //method to define adjective
    Random rand = new Random(); //create the random object
    int randInt = rand.nextInt(10); //generate random integer less than 10
    String adj = "adjective"; //initialize string
    
    switch (randInt){ //define string based on the randomly generated integer
      case 0:
        adj = "good";
        break;
      case 1:
        adj = "little";
        break;
      case 2:
        adj = "blue";
        break;
      case 3:
        adj = "angry";
        break;
      case 4:
        adj = "important";
        break;
      case 5:
        adj = "tired";
        break;
      case 6:
        adj = "clean";
        break;
      case 7:
        adj = "cold";
        break;
      case 8:
        adj = "sparkly";
        break;
      case 9:
        adj = "happy";
        break;
      case 10:
        adj = "sad";
        break;
      default:
        break;
    }
    return adj; //return the adjective to the main method
    }
  
  public static String subject(){ //method for non-primary noun appropriate for subject of sentence
    Random rand = new Random(); //create the random object
    int randInt = rand.nextInt(10); //generate random integer less than 10
    String noun = "noun";
    
    switch (randInt){ //define subject noun based on value of random integer
      case 0:
        noun = "woman";
        break;
      case 1:
        noun = "dog";
        break;
      case 2:
        noun = "cat";
        break;
      case 3:
        noun = "man";
        break;
      case 4:
        noun = "horse";
        break;
      case 5:
        noun = "child";
        break;
      case 6:
        noun = "squirrel";
        break;
      case 7:
        noun = "student";
        break;
      case 8:
        noun = "koala";
        break;
      case 9:
        noun = "dad";
        break;
      case 10:
        noun = "king";
        break;
      default:
        break;
    }
    return noun; //return the noun to the main method
    }
  
  public static String verb(){ //method for past tense verb
    Random rand = new Random(); //create the random object
    int randInt = rand.nextInt(10); //generate random integer less than 10
    String verb = "verb";
    
    switch (randInt){ //define past tense verb based on value of random integer
      case 0:
        verb = "admired";
        break;
      case 1:
        verb = "applauded";
        break;
      case 2:
        verb = "battled";
        break;
      case 3:
        verb = "commanded";
        break;
      case 4:
        verb = "employed";
        break;
      case 5:
        verb = "feared";
        break;
      case 6:
        verb = "loved";
        break;
      case 7:
        verb = "guided";
        break;
      case 8:
        verb = "ignored";
        break;
      case 9:
        verb = "impressed";
        break;
      case 10:
        verb = "remembered";
        break;
      default:
        break;
    }
    return verb; //return the verb to the main method
    }
  
  public static String object(){ //method for non-primary noun appropriate for object of sentence
    Random rand = new Random(); //create the random object
    int randInt = rand.nextInt(10); //generate random integer less than 10
    String noun = "noun";
    
    switch (randInt){ //define subject noun based on value of random integer
      case 0:
        noun = "sponge";
        break;
      case 1:
        noun = "duck";
        break;
      case 2:
        noun = "tree";
        break;
      case 3:
        noun = "pond";
        break;
      case 4:
        noun = "lake";
        break;
      case 5:
        noun = "shovel";
        break;
      case 6:
        noun = "car";
        break;
      case 7:
        noun = "fence";
        break;
      case 8:
        noun = "grass";
        break;
      case 9:
        noun = "food";
        break;
      case 10:
        noun = "building";
        break;
      default:
        break;
    }
    return noun; //return the object to the main method
    }
      
  
  public static void main (String [] args) {
    
    Scanner scan = new Scanner( System.in ); //import scanner
    
    int counter = 0; //initialize counter
    String adj = "adjective"; //initialize all of the strings outside the while loop
    String subject = "subject";
    String verb = "verb";
    String object = "object";
    
    while ( counter == 0 ){ //check if the counter is at 0
      adj = adjective(); //call the adjective function to define adj
      subject = subject(); //call the subject function to define
      verb = verb(); // call verb function to define
      object = object(); //call object funtion to define
      
      System.out.println("The " + adj + " " + subject + " " + verb + " the " + object + "."); //print out sentence
      
      System.out.print("Would you like to generate another sentence? 'yes or no': "); //ask user if they want to play again
      String input = scan.next();
      
      if ( input.equals("no") ){
        counter = 1;
      }
    }
    
  }
}