//Sophie Smith
//CSE02-311
//9-13-2018
//
import java.util.Scanner; //import scanner class to use it
//
public class Convert {
  //main method required for every Java program
  public static void main(String[] args) {
    //collect all the input data
    Scanner myScanner = new Scanner( System.in ); //tell the scanner I am creating an instance that will take input from STDIN
     System.out.print("Enter the affected area in acres: "); //prompt user for the area
      double acres = myScanner.nextDouble(); //call a method to accept the double inputed
    System.out.print("Enter the rainfall in the affected area: "); //prompt user for the rainfall
      double rainfall = myScanner.nextDouble(); //call a method to accept the double inputed
    //develop and print output
    double milesArea; //declare the cubic acres of rain in miles as a double
    double milesRain; //declare the height of rain in miles as a double
      milesArea = acres * .0015625; //convert the acres to miles squared
      milesRain = rainfall * .0000157828; //convert the rainfall to miles
    double cubicMiles = milesArea * milesRain; //calculate the cubic miles of rainfall
    System.out.println(""+cubicMiles+" cubic miles"); //print the cubic miles
  } //end of main method 
} //end of class 