//Sophie Smith
//CSE02-311
//9-13-2018
//
import java.util.Scanner; //import scanner class to use it
//
public class Pyramid {
  //main method required for every Java program
  public static void main(String[] args) {
    //collect all the input data
    Scanner myScanner = new Scanner( System.in ); //tell the scanner I am creating an instance that will take input from STDIN
     System.out.print("The square side of the pyramid is (input length): "); //prompt user for the length of the square side
      double length = myScanner.nextDouble(); //call a method to accept the double inputed
    System.out.print("The height of the pyramid is (input height): "); //prompt user for the height
      double height = myScanner.nextDouble(); //call a method to accept the double inputed
    //calculate and print volume of pyramid
    double volume = (length * length * height) / 3; //calculate volume of pyramid
    System.out.println("The volume inside the pyramid is "+volume+""); //print the volume of the pyramid
  } // end of main method
} // end of class 