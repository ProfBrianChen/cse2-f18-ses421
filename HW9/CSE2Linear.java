//Sophie Smith
//CSE02-311
//11-26-2018
// do binary and linear searches on arrays
//
//
import java.util.Scanner;
import java.util.Random;

public class CSE2Linear {
  
  public static void binary(int[] grades, int search){
    int count = 0; //initialize count to count iterations
    int low = 0; //int low value
    int high = grades.length-1; //int high value 
    boolean found = false;
    
    while( low <= high ){ //while the low is less than the high
      count = count + 1; //add one to iterations
      int mid = low + (high-low) /2; //put the mid right in the middle
      if (grades[mid] < search){ //if the mid is less than the number looking for
        low = mid + 1; //add one to low
      }
      else if (grades[mid] > search){ //if bigger
        high = mid-1; //add one to high
      }
      else if (grades[mid] == search){ //if its equal
        found = true;
        break;
      }
    }
    
    if (found == false){ //print out if not found
    System.out.println(search + " was not found in the list with " + count + " iterations"); //print
    }
    if (found == true){ //print if found
    System.out.println(search + " was found in the list with " + count + " iterations"); //print
    }
  }
  
  public static int[] shuffle(int[] grades){
    Random rand = new Random(); //initialize random machine
    for (int i = 0; i < 100; i++){ //do this 100 times to really shuffle them well
      int zero = grades[0]; //make a int to hold the first element in the int array of grades
      int place = rand.nextInt(15); //randomly select another element in the integer array
      grades[0] = grades[place]; //make the zeroth place the same as the one in the randomly selected place
      grades[place] = zero; //put the first element in the integer array in the place that was randomly selected
    }
    return grades;
  }
  
  public static void linear(int[] grades, int search){
    int count = 0; //start count at 0
    boolean found = false; //boolean to tell if the number has been found
    for (int i = 0; i < 14; i++){ //check each element
      count = count + 1; //add one to count
      if (search == grades[i]){ //if the element is equal to the search
        found = true; //set boolean true
        break; //break out of loop
      }
    }
    if (found == false){ //print out if not found
    System.out.println(search + " was not found in the list with " + count + " iterations"); //print
    }
    if (found == true){ //print if found
    System.out.println(search + " was found in the list with " + count + " iterations"); //print
    }
  }
  
  public static void main ( String[] args){
    
    Scanner scan = new Scanner (System.in);
    int grades[] = new int[15]; //initialize an array to hold the grades
    boolean check = false; //set a boolean variable to check if the number is an integer
    int input = 0; //first input is 0
    
    System.out.print("Enter 15 ascending ints for grade: "); // prompt user to enter
    
    while ( check == false ){ //while the boolean is false
      if (!scan.hasNextInt()){ //check if the user inputted an integer
        System.out.print("That is not an integer. Please enter an integer: "); //ERROR MESSAGE 1
        scan.next(); //scan again
      }
      else{ //if it is an integer
       input = scan.nextInt();  //set the input to the integer entered
        if (input >= 0 && input <= 100){ //if it is between 0 and 100, exit the loop
          check = true; //by setting boolean to true
        }
        else{
          System.out.print("Please enter a number between 0 and 100: "); //ERROR MESSAGE 2
        }
      }
      }
    grades[0] = input;
    
    for(int i = 0; i < 14; i ++){ //build the rest of the array by checking if the number is bigger than the last
      check = false; //reset check to false
      int checknum = grades[i];
      while ( check == false ){ //while the boolean is false
      if (!scan.hasNextInt()){ //check if the user inputted an integer
        System.out.print("That is not an integer. Please enter an integer: "); //ERROR MESSAGE 1
        scan.next(); //scan again
      }
      else{ //if it is an integer
       input = scan.nextInt();  //set the input to the integer entered
        if (input < 0 || input > 100){ //check if the number is out of bounds
          System.out.print("Please enter a number between 0 and 100: "); //ERROR MESSAGE 2
        }
        else if ( input < checknum ){ //check if the numbewr is smaller than the last number
          System.out.print("Please enter a number greater than the last number (" + checknum + "): "); //ERROR MESSAGE 3
        }
        else{ //if neither of these conditions is true, exit the loop
          check = true;
        }
      }
      }
     grades[i+1] = input; //set the next integer in the array equal to the input
    }
    
    for (int i = 0; i < 15; i++){ //print out the finalized array
      System.out.print(grades[i] + " "); //print out the element
    }
    
    System.out.println(" ");
    System.out.print("Enter a grade to search for: "); //have user enter a grade to search for
    int search = scan.nextInt(); // initialize grade to search for
    
    binary(grades,search); //call the binary search method
    
    int[] grades2 = shuffle(grades); //call the shuffle method and set a new array equal to the shuffled array
    
    System.out.println("Scrambled: ");
    for (int i = 0; i < 15; i++){ //print out the finalized array
      System.out.print(grades[i] + " "); //print out the element
    }
    System.out.println(" ");
    
    System.out.print("Enter a grade to search for: "); //search for a grade
    search = scan.nextInt(); //take in the input
    
    linear(grades2, search); //call the linear search method
  }
}