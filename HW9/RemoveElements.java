//Sophie Smith
//CSE02-311
//11-26-2018
// program to create random array, remove and delete particular elements
//
import java.util.Scanner;
import java.util.Random;
import java.util.ArrayList;
import java.util.Arrays;

public class RemoveElements{
  
  public static int[] randomInput(){
    Random rand = new Random(); //make random generator
    int[] array = new int[10]; //initialize array of 10 elements
    System.out.println();
    for (int i = 0; i < 10; i++){ //put elements in the array
      int num = rand.nextInt(10); //randomly generate numbers between 0 and 9
      array[i] = num; //set element equal to random integer
    }
    return array; //return the array to the main method
  }
  
  public static int[] delete(int[] list, int pos){ //delete method
    Scanner scan = new Scanner ( System.in );
    boolean check = false; //set a boolean equal to false
    while (check == false){ //while its false
    if (pos >= 0 && pos <= list.length ){//if pos index is in the list
      check = true; //set it to true
    }
    else{ //otherwise
      System.out.println("Enter an index value between 0 and 9"); //ask for index again
      pos = scan.nextInt();
    }
    }
    
    int array[] = new int[list.length-1]; //make a new array one shorter than the list
    for (int i = 0; i < pos; i++){ //up the the position
      array[i] = list[i]; //set elements equal
    }
    for (int i = pos; i < list.length-1; i++){ //past the position
      array[i] = list[i+1]; //set elements equal to the next element
    }
    
    return array; //return array to the main method
  }
  
  public static int[] remove(int[] list, int target){
    int count = 0;
    for (int i = 0; i< list.length; i++){
      if (list[i] != target){ //if the element is not equal to the target number
        count = count+1; //add one to the counter, counting occurances that aren't the target number
      }
    }
    int list2[] = new int[count]; //make a new array that is the length of the counter
    int count2 = 0; //reset counter to zero to use again
    for(int i = 0; i<list.length; i++){
      if (list[i] != target){ //makes array out of elements that are not equal to the target number
      list2[count2] = list[i]; //fill the array
      count2= count2 + 1; //add one to count so the array goes up by the number of actually filled places

      }
    }
    return list2;
  }
  
  public static void main(String [] arg){
    
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
}
