//compute the cost of items bought at a store including sales tax of 6%
//Sophie Smith, 9/6/2018, CSE02-311
//
public class Arithmetic {
  //main method required for every Java program
  public static void main(String[] args) {
    //pants
    int numPants = 3; //define number of pairs of pants
    double pantsPrice = 34.98; // define cost per pairs of pants
    //shirts
    int numShirts = 2; //define number of shirts
    double shirtsPrice = 24.99; //define price per shirt
    //belts
    int numBelts = 1; //define number of belts
    double beltsPrice = 33.99; //define cost epr belt 
    //tax
    double paSalesTax = 0.06; //define sales tax in PA
    //declare variables for each of the total costs below
    double totalCostOfPants; //total cost of pants, to be calculated later
    double totalCostOfShirts; //total cost of shirts, to be calculated later
    double totalCostOfBelts; //total cost of belts, to be calculated later
    //calculate values to store in total cost variables
    totalCostOfPants = numPants*pantsPrice; //multiply number of pants by price to get total for pants
    totalCostOfShirts = numShirts*shirtsPrice; //multiply number of shirts by price to get total for shirts 
    totalCostOfBelts = numBelts*beltsPrice; //multiply number of belts by price to get total for belts 
    //declare variables for the sales tax charged buying each type of item below
    double salesTaxPants; //total sales tax on pants, to be calculated later
    double salesTaxShirts; //total sales tax on shirts, to be calculated later
    double salesTaxBelts; //total sales tax on belts, to be calculated later
    //calculate values to store in sales tax for each item
    salesTaxPants = totalCostOfPants*paSalesTax; //multiply cost by sales tax to get tax
    salesTaxShirts = totalCostOfShirts*paSalesTax; //multiply cost by sales tax to get tax
    salesTaxBelts = totalCostOfBelts*paSalesTax; //multiply cost by sales tax to get tax
    //print the total cost for each item and the sales tax on each item
    System.out.printf("The total cost for pants is $%.2f", totalCostOfPants); //print cost of pants with 2 decimal points
    System.out.printf(" and the sales tax on pants is $%.2f %n", salesTaxPants); //print sales tax with 2 decimal points)
    System.out.printf("The total cost for shirts is $%.2f", totalCostOfShirts); //print cost of shirts with 2 decimal points
    System.out.printf(" and the sales tax on shirts is $%.2f %n", salesTaxShirts); //print sales tax with 2 decimal points
    System.out.printf("The total cost for belts is $%.2f", totalCostOfBelts); //print cost of belts with 2 decimal points 
    System.out.printf(" and the sales tax for belts is $%.2f %n", salesTaxBelts); //print sales tax with 2 decimal points
    //declare variable for the total cost of purchases before tax
    double totalCostOfGoods; //total cost of all goods before sales tax, to be calculated later
    //calculate total cost of goods
    totalCostOfGoods = totalCostOfPants+totalCostOfShirts+totalCostOfBelts; //add total costs for each item to get total cost pre sales tax
    //print total cost pre sales tax
    System.out.printf("The total cost of the purchases before tax is $%.2f %n", totalCostOfGoods); //print total cost with 2 decimal points
    //declare variable for the total sales tax
    double totalSalesTax; //total sales tax, to be calculated later
    //calculate the total sales tax
    totalSalesTax = salesTaxPants+salesTaxShirts+salesTaxBelts; //add sales tax for each item to get total sales tax
    //print total sales tax
    System.out.printf("The total sales tax on all items is $%.2f %n", totalSalesTax); //print total sales tax to .2 decimals
    //declare variable for the total paid for the transaction, including sales tax
    double transactionTotal; //total amount paid for transaction
    //compute transaction total
    transactionTotal=totalCostOfGoods+totalSalesTax; //add cost of goods to sales tax to get total cost for everything
    //print out transaction total
    System.out.printf("The total cost of the purchases including sales tax is $%.2f %n", transactionTotal); //print transaction total post sales tax with 2 decimal points 
  } //end of main method
} //end of class