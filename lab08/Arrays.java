import java.util.Random;

public class Arrays {
  public static void main ( String[] args){
    Random rand = new Random();
    int[] array1 = new int[100]; //initialize the two arrays
    int[] array2 = new int[10];

    for (int i = 0; i <100; i++){ //fill the array with random integers between 0 and 9
      int num = rand.nextInt(10);
      array1[i]=num;
    }
    //declare numbers that are possible in array
    int count = 0; //initialize a count
    //check array for each of the numbers
for (int x = 0; x < 10; x++){ //count up from 0 to 9
  count = 0;
    for (int i = 0; i< 100; i++){ //count up through entire array
      int num = array1[i]; //initialize integer that is the number of the element in the array
      if (num == x){ //if num equals x
        count = count + 1; //add one to count
      }   
    }
  array2[x]=count; //put the number of occurences in array2
}
    
  System.out.print("Array contains the following integers: ");  
  for (int i = 0; i < 10; i++){ //print out the integer if ouccurences is more than -
    int num = array2[i];
    if (num != 0){
      System.out.print(i + " ");
    }
  }
    System.out.println();
  for (int i = 0; i < 10; i++){ //print out the number of occurences of each number
    int val = array2[i];
    System.out.println(i + " occus " + val + " times.");
  }
    
  }
}