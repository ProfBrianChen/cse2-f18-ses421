//Sophie Smith
//CSE02-311
//9-20-2018
//Create a program that draws a random card and prints the identity
//
public class CardGenerator {
  //main method required for every java program
  public static void main(String[] args) {
    //import random integer values to build card number
    int cardNum = (int)(Math.random() * 100); //gives a random number between 0 and 99
    cardNum = cardNum + 1; //makes sure random number isn't 0
      if ( cardNum > 52 ){
        cardNum = cardNum - 48; //keeps card num at or above 52
      }
    //get real value
    int realNum = ( cardNum % 13 ); //divide by 13 and take the remainder to get the real value of the card, with 0 being a king
    //assign suits
    String cardSuit = "suit"; //declare a variable to hold the suit
    if ( cardNum >=1 && cardNum <= 13){ //diamonds
      cardSuit = "Diamonds";
    }
    if ( cardNum >=14 && cardNum <= 26){ //clubs
      cardSuit = "Clubs";
    }
    if ( cardNum >=27 && cardNum <= 39){ //hearts
      cardSuit = "Hearts";
    }
    if ( cardNum >=40 && cardNum <= 52){ //spades
      cardSuit = "Spades";
    }
    //get card identity
    String cardIdentity = "card identity"; //declare a variable to hold the card identity
    switch ( realNum ){ //look at the real number
      case 0: //if the modulo is 0, it's a king
        cardIdentity = "King";
        break;
      case 1: //if the modulo is 1, it's an ace
        cardIdentity = "Ace";
        break;
      case 11: //if the modulo is 11, it's a jack
        cardIdentity = "Jack";
        break;
      case 12: //if the modulo is 12, it's a queen
        cardIdentity = "Queen";
        break;
      default: //for any other values, the number on the card is the real number calculated above
        cardIdentity = ""+realNum+"";
        break;
    }
    //print
    System.out.println("You picked the " + cardIdentity + " of " + cardSuit); //print the identity combined with the suit
  } //end of main method
} //end of class 