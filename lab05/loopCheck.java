// Sophie Smith
// CSE 02-311
// 9/13/2018
///
/// make a program that reads inputs and checks to see if they are correct
import java.util.Scanner; //import scanner class to use it
//
public class loopCheck {
  // main method required for every Java program
  public static void main(String[] args) {
        Scanner scan = new Scanner( System.in ); //initizlize scanner
    
    //get the course number
    int courseNum;
    System.out.println("What is the number of the course you are taking? "); //ask user
      while( !scan.hasNextInt() ){ //if the scan does not have an integer
        System.out.println("That is not an integer. Please enter an integer.");
        scan.next(); //scan again until it is an integer
      }
    courseNum = scan.nextInt(); //set courseNum equal to integer
    
    //get the department name
    String depName;
    System.out.println("What is the name of the department? "); //ask user
      while( !scan.hasNext() ){ //if the scan does not have a string
        System.out.println("That is not a name. Please enter a name.");
        scan.next(); //scan again until it is a string
      }
    depName = scan.next(); //set depName equal to string
    
    //get the number of times a week
    int numPerWeek;
    System.out.println("How many times does this class meet a week? "); //ask user
      while( !scan.hasNextInt() ){ //if the scan does not have an integer
        System.out.println("That is not an integer. Please enter an integer.");
        scan.next(); //scan again until it is an integer
      }
    numPerWeek = scan.nextInt(); //set numPerWeek equal to integer
    
    //get the start time
    int startTime;
    System.out.println("When does the class start? (HHMM) "); //ask user
      while( !scan.hasNextInt() ){ //if the scan does not have a string
        System.out.println("That is not a time. Please enter a time.");
        scan.next(); //scan again until it is a string
      }
    startTime = scan.nextInt(); //set depName equal to string
    
    //get the instructor name
    String inName;
    System.out.println("What is the name of your instructor? "); //ask user
      while( !scan.hasNext() ){ //if the scan does not have a string
        System.out.println("That is not a name. Please enter a name.");
        scan.next(); //scan again until it is a string
      }
    inName = scan.next(); //set depName equal to string
    
    //get the number of students in class
    int students;
    System.out.println("How many students are in the class? "); //ask user
      while( !scan.hasNextInt() ){ //if the scan does not have an integer
        System.out.println("That is not an integer. Please enter an integer.");
        scan.next(); //scan again until it is an integer
      }
    students = scan.nextInt(); //set numPerWeek equal to integer
    
    
    System.out.println("Course number: " + courseNum);
    System.out.println("Department: " + depName);
    System.out.println("Number of classes per week: " +numPerWeek);
    System.out.println("Start time: " + startTime);
    System.out.println("Instructor name: " + inName);
    System.out.println("Number of students in class: " + students);

  }
}